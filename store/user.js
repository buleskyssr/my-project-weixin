export default {
  // 为当前模块开启命名空间
  namespaced: true,

  // 模块的 state 数据
  state: () => ({
	// 收货地址
	  // address: {}
	  address: JSON.parse(uni.getStorageSync('address') || '{}'),
	  // 登录成功之后的 token 字符串
	  token: uni.getStorageSync('token') || '',
	  // 用户的基本信息
	  userinfo: JSON.parse(uni.getStorageSync('userinfo') || '{}')
  }),

  // 模块的 mutations 方法
  mutations: {
	  // 更新用户的基本信息
	    updateUserInfo(state, userinfo) {
	      state.userinfo = userinfo
	      // 通过 this.commit() 方法，调用 m_user 模块下的 saveUserInfoToStorage 方法，将 userinfo 对象持久化存储到本地
	      this.commit('m_user/saveUserInfoToStorage')
	    },
	  
	    // 将 userinfo 持久化存储到本地
	    saveUserInfoToStorage(state) {
	      uni.setStorageSync('userinfo', JSON.stringify(state.userinfo))
	    },
		updateToken(state,token){
			state.token=token
			this.commit('m_user/saveTokenToStorage')
		},
		saveTokenToStorage(state){
			uni.setStorageSync('token', state.token)
		}
		
  },

  // 模块的 getters 属性
  getters: {},
}