// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import zdyTabbar from "components/zdy-tabbar.vue"
import jsencrypt from 'node_modules/js_sdk/jsencrypt-Rsa/jsencrypt/jsencrypt.vue';
import w_md5 from "node_modules/js_sdk/zww-md5/w_md5.js"
import store from '@/store/store.js'

//注册RSA全局加密
//客户端提交信息加密
uni.$getRSAcode = function(str) { // 注册方法
	let publiukey = `
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQb5UA73PJ/VIli6zoE2q84EAE
zMXZIKROtAwW/7uhNwIeRQRnfrSN/qUjyNhTETExEUFq4s/qUaNA/V4rDo2gtMtI
djAyMBbsmwd7o3xuRZyngTWkG1Ht4oCI5ARosxNZ1N3XFnnMNWqgphnTn8BB6TKA
uS1MqMFl7g4HGP3ZZwIDAQAB
-----END PUBLIC KEY-----`; //引用 rsa 公钥
	//短加密
	var ArrayData = jsencrypt.setEncrypt(publiukey, str);
	return ArrayData;
}

//客户端返回数据信息解密
uni.$getRSADecrypt = function(str) { // 注册方法
	let publiukey = `-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDQydt3j1JANCpl0kndkQJwdoF0jCePkewC9JIpfZOEOLrLTvQN
mlQhAFY9KetrsZMg4hv2SUYLY5bwksNE38XLi3m8pF1XJvodRDrqotNf+4qStKMl
3J4X8CM094z0dKWBxMaCwLMiZtNyGqpbdYY6Gqitx5R6d3Xik6bTBHWOWwIDAQAB
AoGAYr20Lzz2typ0iqcDC+gScUF6Q7Ys13c5WMR1g23CIBv8Kz5xqSU9eEYCgvaa
Qdo/LKQCeV155NT6g1fFRYNWtOvaWHtLdZKnX4SfACUz78pDCPeJnZfAWyE8tWJW
ZXQ9MtIzZWG9w5xW8IEOhz8ohMZpszC3t7MTsb6cTFdL0dECQQDnet6htGozzJ5c
jHpsLKSnqb1BvpaJAb0faEUlnTBe7V11oPvY+lSoJa6ojRP2yiRqqrFxfL5uAYUu
eT/EqGI9AkEA5uenobmWH5ciqUAmI57Pj+tQO3L67BWmHSIpMxarsnMEJqfq3uha
Bq6VG+ytSiwR3SKx5ZJ0AZA1qeoirGG0dwJAVn1DR/ooIFiY5w2aBGcd0oOKclEq
3+AlcOG9zFMSKrXX0Maosya9kOIbg2DLD8/xkWVbzYKFiIbK41VsbSdZJQJBAKwP
JM+THjWuBir+XS0f0uUYYLHb7PRs8Vo2MWMbWpffstEnQRIXK7Zc80lapIpnqmwQ
xWUfK4hasFJL4Mh5CeMCQGwYe5vrnECznk3ix7eGXCDOAIeUBJ15JVFTjCfKOGhf
dtjOkVOkfqqPX3yz1PhvUzj24HJB240RP/JascB/454=
-----END RSA PRIVATE KEY-----`; //引用 rsa 公钥
	//解密
	var openDate = jsencrypt.setDecryptArray(publiukey, str);
	return openDate;
}


// 注册全局组件
Vue.component('zdy-tabbar', zdyTabbar)
//导入网络请求的包
import {
	$http
} from '@escook/request-miniprogram'
uni.$http = $http
//请求的根路径
$http.baseUrl = 'http://localhost:8818'
//请求拦截器
$http.beforeRequest = function(options) {
	uni.showLoading({
		title: '数据加载中...'
	})
}

//响应拦截器
$http.afterRequest = function() {
	uni.hideLoading()
}

//封装弹框方法
uni.$showMsg = function(title = '数据请求失败！', duration = 1500) {
	uni.showToast({
		title,
		duration,
		icon: 'none'
	})
}

//自定义请求头
// 请求开始之前做一些事情
$http.beforeRequest = function(options) {
	var obj = options.data.a
	var time = Math.round(new Date())
	var Ti = "āōêīǐǔǚɑ×÷/∫∮∝∞ξοπρστυφ"
	if (options.url.indexOf('/api/project/v1/users/wxlogin') !== -1) {
		var sign = w_md5.hex_md5_32Upper(options.data.code + time + Ti)
		//console.log(sign)
		options.header = {
			'Sign': sign,
			'Timestamp': time
		
		}
	}else{
		//console.log(obj, time)
		var sign = w_md5.hex_md5_32Upper(options.data.a + time + Ti)
		//console.log(sign)
		options.header = {
			'Sign': sign,
			'Timestamp': time
		
		}
	}
	
	
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import App from './App.vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
